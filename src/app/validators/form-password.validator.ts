import { AbstractControl, ValidationErrors, FormGroup } from '@angular/forms'
 
export function passwordContainsFirstName(fg: FormGroup): ValidationErrors | null {
    const firstNameControl:AbstractControl = fg.get('firstName');
    const passwordControl:AbstractControl = fg.get('password');
    
    if(firstNameControl.valid && passwordControl.value.toLowerCase().includes(firstNameControl.value.toLowerCase())){
        passwordControl.setErrors({containsFirstName: true});
        return { containsFirstName: true }
    }
}

export function passwordContainsLastName(fg: FormGroup): ValidationErrors | null {
    const lastNameControl:AbstractControl = fg.get('lastName');
    const passwordControl:AbstractControl = fg.get('password');
    
    if(lastNameControl.valid && passwordControl.value.toLowerCase().includes(lastNameControl.value.toLowerCase())){
        passwordControl.setErrors({containsLastName: true})
        return { containsLastName: true }
    }
}