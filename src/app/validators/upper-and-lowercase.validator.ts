import { AbstractControl, ValidationErrors } from '@angular/forms'
 
export function hasUpperAndLowerCaseChars(control: AbstractControl): ValidationErrors | null {
    const hasUpperAndLowerCase = control.value.match(/[a-z]/) && control.value.match(/[A-Z]/);
    if(!hasUpperAndLowerCase) return { upperCaseLowerCase: true }
}
