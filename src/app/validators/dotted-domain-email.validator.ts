import { AbstractControl, ValidationErrors } from '@angular/forms'
 
export function isValidDottedDomainEmail(control: AbstractControl): ValidationErrors | null {
    const EMAIL_REGEXP: RegExp  =  /^[a-zA-Z0-9_\.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+$/
    if(!EMAIL_REGEXP.test(control.value)) return { dottedEmail: true }
}
