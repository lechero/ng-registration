import { Component } from '@angular/core';
import { simpleFadeInOutAnimation } from 'app/animations';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  animations: [simpleFadeInOutAnimation]
})
export class LandingComponent {}