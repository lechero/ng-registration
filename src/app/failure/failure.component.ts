import { Component } from '@angular/core';
import { simpleFadeInOutAnimation } from 'app/animations';

@Component({
  selector: 'app-failure',
  templateUrl: './failure.component.html',
  styleUrls: ['./failure.component.scss', '../../pages.scss'],
  animations: [simpleFadeInOutAnimation]
})
export class FailureComponent {}
