import { Component } from '@angular/core';
import { simpleFadeInOutAnimation } from 'app/animations';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss', '../../pages.scss'],
  animations: [simpleFadeInOutAnimation]
})
export class ThankyouComponent {}