import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from 'app/registration/registration.component';
import { LandingComponent } from 'app/landing/landing.component';
import { FailureComponent } from './failure/failure.component';
import { ThankyouComponent } from './thankyou/thankyou.component';

const routes: Routes = [
  { 
    path: '',   
    redirectTo: '/landing',
    pathMatch: 'full' 
  },
  { 
    path: 'landing', 
    component: LandingComponent 
  },
  { 
    path: 'registration', 
    component: RegistrationComponent 
  },
  { 
    path: 'failure', 
    component: FailureComponent 
  },
  { 
    path: 'thankyou', 
    component: ThankyouComponent 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
