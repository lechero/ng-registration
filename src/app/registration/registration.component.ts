import { Component, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { hasUpperAndLowerCaseChars }from 'app/validators/upper-and-lowercase.validator';
import { isValidDottedDomainEmail } from 'app/validators/dotted-domain-email.validator'
import { passwordContainsFirstName, passwordContainsLastName } from 'app/validators/form-password.validator';
import { RegistrationService } from 'app/registration/registration.service';
import { User } from 'app/user';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { simpleFadeInAnimation } from 'app/animations';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  animations: [simpleFadeInAnimation]
})
export class RegistrationComponent {

  public firstName: FormControl = new FormControl('', [Validators.required]);
  public lastName: FormControl = new FormControl('', [Validators.required]);
  public email: FormControl = new FormControl('', [Validators.required, Validators.email, isValidDottedDomainEmail]);
  public password: FormControl = new FormControl('', [Validators.required, Validators.minLength(8), hasUpperAndLowerCaseChars]);

  public hidePassword: boolean = true;
  public isLoading: boolean = false;

  public registrationForm: FormGroup = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      password: this.password
    },
    [
      passwordContainsFirstName,
      passwordContainsLastName
    ]
  );

  @ViewChildren('field') fields:QueryList<ElementRef>;
  
  constructor(private registrationService: RegistrationService, private router: Router) { }

  public getErrorMessage(name: string, formField: FormControl): string | "" {
    if (!formField) return;

    if (formField.hasError('required')) return "Please enter your " + name;
    if (formField.hasError('containsFirstName')) return "Your password may not contain your first name";
    if (formField.hasError('containsLastName')) return "Your password may not contain your last name";
    if (formField.hasError('minlength')) return "A minimum of "+formField.errors.minlength.requiredLength+" characters are needed";
    if (formField.hasError('upperCaseLowerCase')) return "You need uppercase and lowercase characters";
    if (formField.hasError('email')) return "Please enter a valid email address";
    if (formField.hasError('dottedEmail')) return "Please add a domain extension";
    
  }

  public togglePasswordShown(event: any): void {
    event.stopPropagation();
    if(event.detail) {
      this.hidePassword = !this.hidePassword;
    }
    event.preventDefault();
  }

  public onEnter(event: any): void {
    const targetName: string = event.path[0].name;
    const targetFormControl: AbstractControl =   this.registrationForm.controls[targetName];
    if (!targetFormControl.valid) {
        targetFormControl.markAsTouched();
        return;
    }
    const fields: Array<ElementRef> = this.fields.toArray();
    let currentFieldName: string;
    let currentFieldControl: AbstractControl
    for(let field of fields){
        currentFieldName = field.nativeElement.name;
        currentFieldControl = this.registrationForm.controls[currentFieldName];
        if (!currentFieldControl.valid) {
            field.nativeElement.focus();
            event.stopPropagation();
            return;
        }
    }
    this.register();
  }

  public register(): void {
    if (!this.registrationForm.valid) return;

    this.isLoading = true;

    const user: User = {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        email: this.email.value,
        password: this.password.value
    } as User;

    this.registrationService.register(user)
      .pipe(catchError(() =>  of(true)))
      .subscribe((failure) => {
        if(failure) this.router.navigate(["/failure"])
        else this.router.navigate(["/thankyou"])
      })
  }

}
