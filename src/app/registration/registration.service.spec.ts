import { TestBed } from '@angular/core/testing';

import { RegistrationService } from 'app/registration/registration.service';

describe('RegistrationServiceService', () => {
  let service: RegistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegistrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
